# -*- coding: utf-8 -*-

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale, QMetaObject, QObject, QPoint, QRect, QSize,
							QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont, QFontDatabase, QGradient, QIcon, QImage,
							QKeySequence, QLinearGradient, QPainter, QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QLabel, QLineEdit, QMainWindow, QPushButton,
								QSizePolicy, QSlider, QSpacerItem, QSpinBox, QVBoxLayout, QWidget, QAbstractSlider)
from battery import Ui_Battery
import sys


class battery_main(QMainWindow, Ui_Battery):

	def __init__(self):
		QMainWindow.__init__(self)
		# set up User Interface (widgets, layout...)
		self.setupUi(self)
		self.count = 0

	def slot1(self):
		self.count += 1
		if self.count <= 1:
			print('slot1', 'you triggered ', self.count, ' time.')
		else:
			print('slot1', 'you triggered ', self.count, ' times.')

	def lower_ref_current(self):
		self.ref_current_slider.triggerAction(QAbstractSlider.SliderSingleStepSub)

	def raise_ref_current(self):
		self.ref_current_slider.triggerAction(QAbstractSlider.SliderSingleStepAdd)

	def power_on_off(self, on_off):
		print('power_on_off\n' + str(on_off))

	def apply(self):
		print('apply')

	def switch_mode(self, mode):
		print('switch_mode')
		print(mode)


if __name__ == "__main__":
	print(sys.api_version, sys.argv)

	app = QApplication(sys.argv)
	app.setQuitOnLastWindowClosed(True)

	battery0_window = battery_main()
	battery0_window.show()
	battery0_window.title.setStyleSheet("\
                QPushButton {   \
                    color:white;    \
                    border-style: outset;  \
                    background-color: rgb(20, 80, 200);\
                }   \
                QPushButton:checked{\
                    background-color: grey;\
                    border: none; \
                }\
                QPushButton:hover{  \
                    color: white ; \
                    background-color: green; \
                    border-style: outset;  \
                }  \
                ")
	# Start the app up
	sys.exit(app.exec_())
