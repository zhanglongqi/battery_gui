# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'battery.ui'
##
## Created by: Qt User Interface Compiler version 6.7.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QLabel,
    QLineEdit, QMainWindow, QPushButton, QSizePolicy,
    QSlider, QSpacerItem, QSpinBox, QVBoxLayout,
    QWidget)

class Ui_Battery(object):
    def setupUi(self, Battery):
        if not Battery.objectName():
            Battery.setObjectName(u"Battery")
        Battery.setWindowModality(Qt.ApplicationModal)
        Battery.resize(800, 400)
        self.centralWidget = QWidget(Battery)
        self.centralWidget.setObjectName(u"centralWidget")
        self.centralWidget.setEnabled(True)
        self.centralWidget.setMinimumSize(QSize(639, 300))
        self.verticalLayout_5 = QVBoxLayout(self.centralWidget)
        self.verticalLayout_5.setSpacing(6)
        self.verticalLayout_5.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.title = QPushButton(self.centralWidget)
        self.title.setObjectName(u"title")
        font = QFont()
        font.setPointSize(20)
        self.title.setFont(font)
        self.title.setFlat(True)

        self.verticalLayout_4.addWidget(self.title)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(6)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(-1, 10, -1, 10)
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setSpacing(6)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.ref_curent_spinBox = QSpinBox(self.centralWidget)
        self.ref_curent_spinBox.setObjectName(u"ref_curent_spinBox")
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ref_curent_spinBox.sizePolicy().hasHeightForWidth())
        self.ref_curent_spinBox.setSizePolicy(sizePolicy)
        font1 = QFont()
        font1.setPointSize(15)
        self.ref_curent_spinBox.setFont(font1)
        self.ref_curent_spinBox.setAlignment(Qt.AlignBottom|Qt.AlignHCenter)
        self.ref_curent_spinBox.setMaximum(2000)

        self.horizontalLayout_3.addWidget(self.ref_curent_spinBox)

        self.label_7 = QLabel(self.centralWidget)
        self.label_7.setObjectName(u"label_7")
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setFont(font1)
        self.label_7.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.horizontalLayout_3.addWidget(self.label_7)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.low_ref_current_btn = QPushButton(self.centralWidget)
        self.low_ref_current_btn.setObjectName(u"low_ref_current_btn")
        self.low_ref_current_btn.setEnabled(True)
        sizePolicy1 = QSizePolicy(QSizePolicy.Policy.Maximum, QSizePolicy.Policy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.low_ref_current_btn.sizePolicy().hasHeightForWidth())
        self.low_ref_current_btn.setSizePolicy(sizePolicy1)
        font2 = QFont()
        font2.setPointSize(36)
        self.low_ref_current_btn.setFont(font2)
        self.low_ref_current_btn.setAutoDefault(False)
        self.low_ref_current_btn.setFlat(False)

        self.horizontalLayout_2.addWidget(self.low_ref_current_btn)

        self.ref_current_slider = QSlider(self.centralWidget)
        self.ref_current_slider.setObjectName(u"ref_current_slider")
        sizePolicy2 = QSizePolicy(QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.ref_current_slider.sizePolicy().hasHeightForWidth())
        self.ref_current_slider.setSizePolicy(sizePolicy2)
        self.ref_current_slider.setMaximum(2000)
        self.ref_current_slider.setOrientation(Qt.Horizontal)

        self.horizontalLayout_2.addWidget(self.ref_current_slider)

        self.raise_ref_current_btn = QPushButton(self.centralWidget)
        self.raise_ref_current_btn.setObjectName(u"raise_ref_current_btn")
        sizePolicy1.setHeightForWidth(self.raise_ref_current_btn.sizePolicy().hasHeightForWidth())
        self.raise_ref_current_btn.setSizePolicy(sizePolicy1)
        self.raise_ref_current_btn.setFont(font2)
        self.raise_ref_current_btn.setFlat(False)

        self.horizontalLayout_2.addWidget(self.raise_ref_current_btn)


        self.verticalLayout_2.addLayout(self.horizontalLayout_2)


        self.verticalLayout_3.addLayout(self.verticalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 10, -1, 10)
        self.label = QLabel(self.centralWidget)
        self.label.setObjectName(u"label")
        self.label.setFont(font1)
        self.label.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.label)

        self.line = QFrame(self.centralWidget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.Shape.VLine)
        self.line.setFrameShadow(QFrame.Shadow.Sunken)

        self.horizontalLayout.addWidget(self.line)

        self.label_2 = QLabel(self.centralWidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font1)
        self.label_2.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.label_2)

        self.constant_current_LED = QPushButton(self.centralWidget)
        self.constant_current_LED.setObjectName(u"constant_current_LED")
        self.constant_current_LED.setEnabled(True)
        sizePolicy.setHeightForWidth(self.constant_current_LED.sizePolicy().hasHeightForWidth())
        self.constant_current_LED.setSizePolicy(sizePolicy)
        self.constant_current_LED.setFont(font1)

        self.horizontalLayout.addWidget(self.constant_current_LED)

        self.label_3 = QLabel(self.centralWidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setFont(font1)
        self.label_3.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.label_3)

        self.monitor_but_LED_btn = QPushButton(self.centralWidget)
        self.monitor_but_LED_btn.setObjectName(u"monitor_but_LED_btn")
        self.monitor_but_LED_btn.setEnabled(True)
        sizePolicy.setHeightForWidth(self.monitor_but_LED_btn.sizePolicy().hasHeightForWidth())
        self.monitor_but_LED_btn.setSizePolicy(sizePolicy)
        self.monitor_but_LED_btn.setFont(font1)

        self.horizontalLayout.addWidget(self.monitor_but_LED_btn)


        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setSpacing(6)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(-1, 10, -1, 10)
        self.label_4 = QLabel(self.centralWidget)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font1)

        self.horizontalLayout_4.addWidget(self.label_4)

        self.power_lineEdit = QLineEdit(self.centralWidget)
        self.power_lineEdit.setObjectName(u"power_lineEdit")
        self.power_lineEdit.setFont(font1)

        self.horizontalLayout_4.addWidget(self.power_lineEdit)

        self.label_5 = QLabel(self.centralWidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font1)

        self.horizontalLayout_4.addWidget(self.label_5)

        self.label_6 = QLabel(self.centralWidget)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font1)

        self.horizontalLayout_4.addWidget(self.label_6)

        self.mode_lineEdit = QLineEdit(self.centralWidget)
        self.mode_lineEdit.setObjectName(u"mode_lineEdit")
        self.mode_lineEdit.setFont(font1)

        self.horizontalLayout_4.addWidget(self.mode_lineEdit)


        self.verticalLayout_3.addLayout(self.horizontalLayout_4)


        self.horizontalLayout_5.addLayout(self.verticalLayout_3)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.apply_btn = QPushButton(self.centralWidget)
        self.apply_btn.setObjectName(u"apply_btn")
        sizePolicy.setHeightForWidth(self.apply_btn.sizePolicy().hasHeightForWidth())
        self.apply_btn.setSizePolicy(sizePolicy)
        self.apply_btn.setFont(font1)
        self.apply_btn.setIconSize(QSize(32, 32))

        self.verticalLayout.addWidget(self.apply_btn)

        self.power_btn = QPushButton(self.centralWidget)
        self.power_btn.setObjectName(u"power_btn")
        sizePolicy.setHeightForWidth(self.power_btn.sizePolicy().hasHeightForWidth())
        self.power_btn.setSizePolicy(sizePolicy)
        self.power_btn.setFont(font1)
        self.power_btn.setIconSize(QSize(32, 32))
        self.power_btn.setCheckable(True)

        self.verticalLayout.addWidget(self.power_btn)

        self.mode_btn = QPushButton(self.centralWidget)
        self.mode_btn.setObjectName(u"mode_btn")
        sizePolicy.setHeightForWidth(self.mode_btn.sizePolicy().hasHeightForWidth())
        self.mode_btn.setSizePolicy(sizePolicy)
        self.mode_btn.setFont(font1)
        self.mode_btn.setCheckable(True)

        self.verticalLayout.addWidget(self.mode_btn)


        self.horizontalLayout_5.addLayout(self.verticalLayout)


        self.verticalLayout_4.addLayout(self.horizontalLayout_5)


        self.verticalLayout_5.addLayout(self.verticalLayout_4)

        Battery.setCentralWidget(self.centralWidget)
        QWidget.setTabOrder(self.low_ref_current_btn, self.ref_current_slider)
        QWidget.setTabOrder(self.ref_current_slider, self.raise_ref_current_btn)
        QWidget.setTabOrder(self.raise_ref_current_btn, self.ref_curent_spinBox)
        QWidget.setTabOrder(self.ref_curent_spinBox, self.apply_btn)
        QWidget.setTabOrder(self.apply_btn, self.power_btn)
        QWidget.setTabOrder(self.power_btn, self.mode_btn)
        QWidget.setTabOrder(self.mode_btn, self.constant_current_LED)
        QWidget.setTabOrder(self.constant_current_LED, self.monitor_but_LED_btn)
        QWidget.setTabOrder(self.monitor_but_LED_btn, self.power_lineEdit)
        QWidget.setTabOrder(self.power_lineEdit, self.mode_lineEdit)
        QWidget.setTabOrder(self.mode_lineEdit, self.title)

        self.retranslateUi(Battery)
        self.low_ref_current_btn.pressed.connect(Battery.lower_ref_current)
        self.raise_ref_current_btn.clicked.connect(Battery.raise_ref_current)
        self.ref_current_slider.valueChanged.connect(self.ref_curent_spinBox.setValue)
        self.power_btn.toggled.connect(Battery.power_on_off)
        self.apply_btn.clicked.connect(Battery.apply)
        self.mode_btn.toggled.connect(Battery.switch_mode)

        self.low_ref_current_btn.setDefault(False)


        QMetaObject.connectSlotsByName(Battery)
    # setupUi

    def retranslateUi(self, Battery):
        Battery.setWindowTitle(QCoreApplication.translate("Battery", u"Battery", None))
        self.title.setText(QCoreApplication.translate("Battery", u"Lithium Ion Battery", None))
        self.label_7.setText(QCoreApplication.translate("Battery", u"mA", None))
        self.low_ref_current_btn.setText(QCoreApplication.translate("Battery", u"<", None))
        self.raise_ref_current_btn.setText(QCoreApplication.translate("Battery", u">", None))
        self.label.setText(QCoreApplication.translate("Battery", u"Operation \n"
" Stage:", None))
        self.label_2.setText(QCoreApplication.translate("Battery", u"Constant \n"
" Curent:", None))
        self.constant_current_LED.setText(QCoreApplication.translate("Battery", u"LED0", None))
        self.label_3.setText(QCoreApplication.translate("Battery", u"Monitor\n"
"Bus:", None))
        self.monitor_but_LED_btn.setText(QCoreApplication.translate("Battery", u"LED1", None))
        self.label_4.setText(QCoreApplication.translate("Battery", u"Power:", None))
        self.label_5.setText(QCoreApplication.translate("Battery", u"KW", None))
        self.label_6.setText(QCoreApplication.translate("Battery", u"Mode:", None))
        self.apply_btn.setText(QCoreApplication.translate("Battery", u"  Apply  ", None))
        self.power_btn.setText(QCoreApplication.translate("Battery", u"ON/OFF", None))
        self.mode_btn.setText(QCoreApplication.translate("Battery", u"Switch Mode", None))
    # retranslateUi

