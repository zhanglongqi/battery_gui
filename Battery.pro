#-------------------------------------------------
#
# Project created by QtCreator 2015-08-20T17:30:51
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Battery
TEMPLATE = app


SOURCES += main.cpp\
        battery.cpp

HEADERS  += battery.h

FORMS    += battery.ui \
    test.ui

RESOURCES += \
    image.qrc

DISTFILES += \
    battery_main.py

STATECHARTS += \
    statemachine.scxml
