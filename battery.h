#ifndef BATTERY_H
#define BATTERY_H

#include <QMainWindow>

namespace Ui {
class Battery;
}

class Battery : public QMainWindow
{
    Q_OBJECT

public:
    explicit Battery(QWidget *parent = 0);
    ~Battery();

private:
    Ui::Battery *ui;

public slots:
    void lower_ref_current();
    void raise_ref_current();

};

#endif // BATTERY_H
